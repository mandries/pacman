# Pacman

Pacman game clone written in Java  
Author: **Mihai Andries**  
Date: April-May 2009  
Université de Strasbourg

To run:
1.  Import the project into Eclipse Java IDE
2.  Open src/Play.java
3.  Run the game