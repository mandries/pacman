public class Level 
{	
	/*
	 * Values used in the game-world tables
	 * 0 = Wall
	 * 1 = Empty
	 * 2 = Dot
	 * 3 = Cherry
	 */

	// Constructor for an empty level
	public Level()
	{		
		for(int j=0;j<this.height;j++)
			for (int i=0;i<this.width;i++)
				{this.map[i][j]=1;}
	}
		
	// Coordinates where pacman appears at level start
	private int pacmanXOrigin;
	public int getPacmanXOrigin()
	{return pacmanXOrigin;}
	public void setPacmanXOrigin(int origin) 
	{pacmanXOrigin = origin;}

	private int pacmanYOrigin;
	public int getPacmanYOrigin()
	{return pacmanYOrigin;}
	public void setPacmanYOrigin(int origin) 
	{pacmanYOrigin = origin;}

	// Coordinates where phantoms appear at level start
	private  int phantomXOrigin;
	public int getPhantomXOrigin() 
	{return phantomXOrigin;}
	public void setPhantomXOrigin(int origin) 
	{phantomXOrigin = origin;}
	
	private int phantomYOrigin;
	public int getPhantomYOrigin() 
	{return phantomYOrigin;}
	public void setPhantomYOrigin(int origin) 
	{phantomYOrigin = origin;}

	// Cell dimensions
	private int cellHeight=40;
	public int getCellHeight() 
		{return cellHeight;	}

	private int cellWidth=40;
	public int getCellWidth() 
		{return cellWidth;	}
	
	// Gameworld dimensions
	private int width=20;
	public int getWidth() 
		{return width;}

	private int height=15;
	public int getHeight() 
		{return height;	}
	
	// protected int[][] map = new int [width][height];
	private int[][] map = new int [20][15];	

	public void setCellValue(int xCoord, int yCoord, int value)
	{
		this.map[(xCoord/this.cellWidth + this.width)%this.width][(yCoord/this.cellHeight + this.height)%this.height]=value;
	}
	
	public int getCellValue(int xCoordtab, int yCoordtab)
	{
		return this.map[(xCoordtab + this.width)%this.width][(yCoordtab + this.height)%this.height];
	}

	// Function used to determine if the next level has been reached or not
	public boolean existDots()
	{
		for (int j=0;j<height;j++)
			for (int i=0;i<width;i++)
			{
				if (this.map[i][j]>1)
					return true;
			}
		return false;
	}
	
	// Map creation for the first level
	public void firstLevel()
	{	
	
		// Map Border
		for (int j=0;j<this.height;j++)
			for (int i=0;i<this.width;i++)
				{
					if (j==0 || i==0 || i==this.width-1 || j==this.height-1)
						{this.map[i][j]=0;}
					else {this.map[i][j]=2;}
				}
		this.map[9][1]=0;	this.map[10][1]=0;			
		this.map[2][2]=0;	this.map[3][2]=0;	this.map[5][2]=0;	this.map[6][2]=0;	this.map[7][2]=0;
		this.map[9][2]=0;	this.map[10][2]=0;	this.map[12][2]=0;	this.map[13][2]=0;	this.map[14][2]=0;	this.map[16][2]=0;	this.map[17][2]=0;
		this.map[9][3]=0;	this.map[10][3]=0;
		this.map[2][4]=0;	this.map[3][4]=0;	this.map[5][4]=0;
		this.map[7][4]=0;	this.map[8][4]=0;	this.map[9][4]=0;	this.map[10][4]=0;	this.map[11][4]=0;	this.map[12][4]=0;
		this.map[14][4]=0;	this.map[16][4]=0;	this.map[17][4]=0;
		this.map[1][6]=0;	this.map[2][6]=0;	this.map[3][6]=0;	this.map[5][6]=0;	this.map[6][6]=0;
		this.map[8][6]=0;	this.map[11][6]=0;	this.map[13][6]=0;	this.map[14][6]=0;	this.map[16][6]=0;	this.map[17][6]=0;	this.map[18][6]=0;
		this.map[5][7]=0;	this.map[6][7]=0;
		this.map[8][7]=0;	this.map[11][7]=0;	this.map[13][7]=0;	this.map[14][7]=0;
		this.map[1][8]=0;	this.map[2][8]=0;	this.map[3][8]=0;	this.map[5][8]=0;	this.map[6][8]=0;
		this.map[8][8]=0;	this.map[9][8]=0;	this.map[10][8]=0;	this.map[11][8]=0;	this.map[13][8]=0;	this.map[14][8]=0;	this.map[16][8]=0;	this.map[17][8]=0;	this.map[18][8]=0;
		this.map[2][10]=0;	this.map[3][10]=0;	this.map[4][10]=0;	this.map[6][10]=0;	this.map[8][10]=0;	this.map[9][10]=0;	this.map[10][10]=0;	this.map[11][10]=0;
		this.map[13][10]=0;	this.map[15][10]=0;	this.map[16][10]=0;	this.map[17][10]=0;
		this.map[6][11]=0;	this.map[13][11]=0;	this.map[9][11]=0;	this.map[10][11]=0;
		this.map[2][12]=0;	this.map[3][12]=0;	this.map[5][12]=0;	this.map[6][12]=0;	this.map[7][12]=0;	this.map[9][12]=0;	this.map[10][12]=0;	this.map[12][12]=0;	this.map[13][12]=0;	this.map[14][12]=0;	this.map[16][12]=0;	this.map[17][12]=0;
		this.map[9][13]=0;	this.map[10][13]=0;	
		this.map[9][6]=0;	this.map[10][6]=1;
		//Fruits
		this.map[1][10]=3; this.map[18][10]=3;
		this.map[1][2]=3;this.map[18][2]=3;
		
		// Phantom lair
		this.map[9][7]=1; this.map[10][7]=1;
		// Teleports
		this.map[0][7]=1; this.map[19][7]=1;	
	}// end of map creation for the first level
	
	
	// Map creation for the second level
	public void secondLevel()
	{	
	
		// Map Border
		for (int j=0;j<this.height;j++)
			for (int i=0;i<this.width;i++)
				{
					if (j==0 || i==0 || i==this.width-1 || j==this.height-1)
						{this.map[i][j]=0;}
					else {this.map[i][j]=2;}
				}
		
		// Walls
		this.map[7][1]=0;	this.map[9][1]=0;	this.map[10][1]=0;	this.map[17][1]=0;	this.map[18][1]=0;	this.map[19][11]=0;
		this.map[2][2]=0;	this.map[3][2]=0;	this.map[5][2]=0;	this.map[6][2]=0;	this.map[7][2]=0;	this.map[12][2]=0;	this.map[14][2]=0;	this.map[15][2]=0;
		this.map[2][3]=0;	this.map[9][3]=0;	this.map[11][3]=0;	this.map[12][3]=0;	this.map[15][3]=0;	this.map[16][3]=0;	this.map[17][3]=0;
		this.map[2][4]=0;	this.map[3][4]=0;	this.map[5][4]=0;	this.map[6][4]=0;	this.map[7][4]=0;	this.map[8][4]=0;	this.map[9][4]=0;	this.map[11][4]=0;	this.map[14][4]=0;	this.map[15][4]=0;
		this.map[3][5]=0;	this.map[6][5]=0;	this.map[13][5]=0;	this.map[14][5]=0;	this.map[17][5]=0;
		this.map[2][6]=0;	this.map[3][6]=0;	this.map[5][6]=0;	this.map[6][6]=0;	this.map[8][6]=0;	this.map[9][6]=0;	this.map[10][6]=0;	this.map[11][6]=0;	this.map[13][6]=0;	this.map[16][6]=0;	this.map[17][6]=0;
		this.map[2][7]=0;	this.map[6][7]=0;	this.map[8][7]=0;	this.map[11][7]=0;	this.map[15][7]=0;	this.map[16][7]=0;	this.map[17][7]=0;
		this.map[2][8]=0;	this.map[3][8]=0;	this.map[5][8]=0;	this.map[6][8]=0;	this.map[8][8]=0;	this.map[10][8]=0;	this.map[11][8]=0;	this.map[12][8]=0;	this.map[13][8]=0;	this.map[15][8]=0;	this.map[16][8]=0;	this.map[17][8]=0;
		this.map[6][9]=0;	this.map[10][9]=0;
		this.map[2][10]=0;	this.map[3][10]=0;	this.map[5][10]=0;	this.map[6][10]=0;	this.map[8][10]=0;	this.map[9][10]=0;	this.map[10][10]=0;	this.map[11][10]=0;	this.map[13][10]=0;	this.map[15][10]=0;	this.map[16][10]=0;	this.map[17][10]=0;
		this.map[2][11]=0;	this.map[6][11]=0;	this.map[9][11]=0;	this.map[15][11]=0;
		this.map[2][12]=0;	this.map[3][12]=0;	this.map[5][12]=0;	this.map[6][12]=0;	this.map[7][12]=0;	this.map[9][12]=0;	this.map[11][12]=0;	this.map[13][12]=0;	this.map[14][12]=0;	this.map[15][12]=0;	this.map[17][12]=0;
		this.map[5][13]=0;	this.map[11][13]=0;
		
		//Fruits
		this.map[3][7]=3;
		this.map[14][3]=3;
		this.map[6][13]=3;
		// Phantom lair
		this.map[9][7]=1; this.map[10][7]=1;
		
		// Teleports
		this.map[4][0]=1; this.map[4][14]=1;
		this.map[0][2]=1; this.map[19][2]=1;
	}// end of map creation for the second level
	
	// Map creation for the third level
	public void thirdLevel()
	{	
	
		// Map Border
		for (int j=0;j<this.height;j++)
			for (int i=0;i<this.width;i++)
				{
					if (j==0 || i==0 || i==this.width-1 || j==this.height-1)
						{this.map[i][j]=0;}
					else {this.map[i][j]=2;}
				}
		
		// Walls
		this.map[10][1]=0;	this.map[11][1]=0;	this.map[12][1]=0;
		this.map[2][2]=0;	this.map[3][2]=0;	this.map[5][2]=0;	this.map[6][2]=0;	this.map[8][2]=0;	this.map[12][2]=0;	this.map[14][2]=0;	this.map[15][2]=0;	this.map[16][2]=0;	this.map[17][2]=0;
		this.map[2][3]=0;	this.map[3][3]=0;	this.map[8][3]=0;	this.map[9][3]=0;	this.map[10][3]=0;	this.map[12][3]=0;	this.map[17][3]=0;
		this.map[2][4]=0;	this.map[3][4]=0;	this.map[4][4]=0;	this.map[6][4]=0;	this.map[7][4]=0;	this.map[8][4]=0;	this.map[10][4]=0;	this.map[12][4]=0;	this.map[13][4]=0;	this.map[14][4]=0;	this.map[15][4]=0;	this.map[17][4]=0;
		this.map[4][5]=0;
		this.map[1][6]=0;	this.map[2][6]=0;	this.map[4][6]=0;	this.map[6][6]=0;	this.map[7][6]=0;	this.map[8][6]=0;	this.map[10][6]=0;	this.map[11][6]=0;	this.map[13][6]=0;	this.map[14][6]=0;	this.map[15][6]=0;	this.map[16][6]=0;	this.map[17][6]=0;
		this.map[7][7]=0;	this.map[11][7]=0;	this.map[13][7]=0;
		this.map[2][8]=0;	this.map[4][8]=0;	this.map[5][8]=0;	this.map[7][8]=0;	this.map[8][8]=0;	this.map[9][8]=0;	this.map[10][8]=0;	this.map[11][8]=0;	this.map[13][8]=0;	this.map[15][8]=0;	this.map[17][8]=0;
		this.map[4][9]=0;
		this.map[2][10]=0;	this.map[3][10]=0;	this.map[4][10]=0;	this.map[6][10]=0;	this.map[7][10]=0;	this.map[9][10]=0;	this.map[10][10]=0;	this.map[12][10]=0;	this.map[14][10]=0;	this.map[15][10]=0;	this.map[16][10]=0;	this.map[17][10]=0;
		this.map[2][11]=0;	this.map[3][11]=0;	this.map[4][11]=0;	this.map[7][11]=0;	this.map[9][11]=0;	this.map[12][11]=0;	this.map[14][11]=0;
		this.map[4][12]=0;	this.map[5][12]=0;	this.map[9][12]=0;	this.map[11][12]=0;	this.map[12][12]=0;	this.map[14][12]=0;	this.map[16][12]=0;	this.map[17][12]=0;
		this.map[2][13]=0;	this.map[7][13]=0;
		
		//Fruits
		this.map[3][7]=3;
		this.map[14][3]=3;
		this.map[6][13]=3;
		// Phantom lair
		this.map[9][6]=1; 
		this.map[8][7]=1;	this.map[9][7]=1;	this.map[10][7]=1;
		
		// Teleports
		this.map[9][0]=1;
		this.map[0][5]=1; this.map[19][5]=1;
		this.map[0][11]=1; this.map[19][11]=1;
		this.map[9][14]=1;
	}

	
}	// end of Level class
