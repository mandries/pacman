public class Phantom extends Person 
{
	/*	Difficulty:
	 * 	- Number of phantoms
	 * 	- Phantom speed
	 * 	- Phantom brains
	 */
	private int speed=1;
	public int getSpeed()
	{return this.speed;}
	public void setSpeed(int value)
	{this.speed=value;}
	public void incSpeed()
	{	
		if (this.speed>1)
			this.speed++;
	}
	
	public Phantom()
	{super();}
	
	public Phantom(int xCoord, int yCoord)
	{super(xCoord,yCoord);}
	
	private boolean dead=false;
	public boolean isDead()
		{return dead;}
	public void switchDead() 
		{this.dead = !this.dead;}
		
	private int[][] path = new int [20][15];
	
	// Timer showing if this phantoms is a pray
	private int cherryTimer=0;
	public int getCherryTimer()
	{return cherryTimer;}
	public void setCherryTimer(int value)
	{cherryTimer=value;}
	public void decreaseCherryTimer()
	{cherryTimer--;}

	// Timer keeping the phantom inside it's home
	private int releaseTimer=0;
	public int getReleaseTimer()
	{return releaseTimer;}
	public void setReleaseTimer(int value)
	{releaseTimer=value;}
	
	// Timer used to show the awarded points for killing a phantom
	private int awardTimer;
	public int getAwardTimer()
	{return awardTimer;}
	public void setAwardTimer(int value)
	{awardTimer=value;}
	public void decAwardTimer()
	{awardTimer--;}
	
	// Value used to show how many points were awarded, depending on the number of phantoms killed in a row
	private int awardScore=200;
	public void setAwardScore(int value)
	{awardScore=value;}
	public int getAwardScore()
	{return awardScore;}
	
	// Placement where the phantom has been killed
	private int xKilled=0;
	public int getXKilled() 
	{return xKilled;}
	public void setXKilled(int value) 
	{xKilled = value;}
	
	private int yKilled=0;
	public int getYKilled() 
	{return yKilled;}
	public void setYKilled(int value) 
	{yKilled = value;}
	
	//  Function used to move the phantom
	public void movePhantom(Level level)
	{
		int xCoordtab=this.xCoord/level.getCellWidth();
		int yCoordtab=this.yCoord/level.getCellHeight();
		int width=level.getWidth();
		int height=level.getHeight();

		int value=0;
		// if phantom = hunter --> catch pacman
		if (this.cherryTimer == 0)
			{value=this.minSurrounding(level);}
		// if phantom = pray --> run away from pacman
		else 
			{value=this.maxSurrounding(level);}
		int yoffs[]={-1,0,1,0};
		int xoffs[]={0,1,0,-1};
		
		// choosing the path (max if running, min if chasing)
		for (int j=0;j<4;j++)
		{
		   int nx=(xCoordtab+xoffs[j]+width)%width, ny=(yCoordtab+yoffs[j]+height)%height;
		   if (value==this.path[nx][ny])
			{
				if (nx==((this.xCoord/level.getCellWidth())+1)%width)
					{this.movePerson(level, this.moveDirection=java.awt.event.KeyEvent.VK_RIGHT); j=4;}
				else if (nx==(this.xCoord/level.getCellWidth()-1+width)%width)
					{this.movePerson(level, this.moveDirection=java.awt.event.KeyEvent.VK_LEFT);j=4;}
				else if (ny==(this.yCoord/level.getCellHeight()-1+height)%height)
					{this.movePerson(level, this.moveDirection=java.awt.event.KeyEvent.VK_UP);j=4;}
				else if (ny==(this.yCoord/level.getCellHeight()+1)%height)
					{this.movePerson(level, this.moveDirection=java.awt.event.KeyEvent.VK_DOWN);j=4;}
			}
		}
	}	// end of method movePhantom
	
	
	
	// Calculate the MIN value of the surrounding cell
	public int minSurrounding(Level level)
	{
		int height = level.getHeight();
		int width = level.getWidth();
		int xCoord=this.getXCoord()/level.getCellWidth();
		int yCoord=this.getYCoord()/level.getCellHeight();
		return Math.min(Math.min(this.path[xCoord][(yCoord-1+height)%height], this.path[xCoord][(yCoord+1)%height]),
						Math.min(this.path[(xCoord-1+width)%width][yCoord], this.path[(xCoord+1)%width][yCoord]));
		
	}// end of method minSurrounding
	
	// Function used to calculate the surrounding max, eliminating walls from the calculations 
	public int exceptWallMax(Level level, int a, int b)
	{
		// if a==wall
		if (a==level.getWidth()*level.getHeight()+1)
		{
			// if b==wall then 0
			if (b==level.getWidth()*level.getHeight()+1)
				{return 0;}
			// else b
			else
			{
				return b;
			}
		// if a is not wall
		}
		else
		{
			// if b==wall
			if (b==level.getWidth()*level.getHeight()+1)
			{return a;}
			// if b is not a wall
			else return Math.max(a,b); 
		}
	}// end of method exceptWallMax
	
	
	// Calculate the MAX value of the surrounding cells
	public int maxSurrounding(Level level)
	{
		int xCoord=this.getXCoord()/level.getCellWidth();
		int yCoord=this.getYCoord()/level.getCellHeight();
		int height=level.getHeight();
		int width =level.getWidth();
		return Math.max(exceptWallMax(level,this.path[xCoord][(yCoord-1+height)%height], this.path[xCoord][(yCoord+1)%height]),
						exceptWallMax(level, this.path[(xCoord-1+width)%width][yCoord], this.path[(xCoord+1)%width][yCoord]));
	}// end of method maxSurrounding
	
	
	// Initialize the function for the path calculus
	public void initPacPath(Level level, Protagonist pacman)
	{
		int height=level.getHeight();
		int width =level.getWidth();
		int xOrigin = level.getPhantomXOrigin();
		int yOrigin = level.getPhantomYOrigin();
	
			// Copy level.map to phantom.path
			// Walls = max+1
			// Free cells = max;
			for (int j=0; j<height; j++)
				for (int i=0; i<width; i++)			
				{
					if (level.getCellValue(i,j)!=0)	// if not wall
						{this.path[i][j]=height*width;}
					else // if wall
						{this.path[i][j]=height*width+1;}
				}	
	

			// Establish 0 as the pacman place IF NOT DEAD
			if (!this.isDead())
				{
					// Hold phantoms in house to separate them
					if (this.releaseTimer>0)
					{
						this.path[xOrigin/level.getCellWidth()][yOrigin/level.getCellHeight()]=0;
						releaseTimer--;
					}
					else
					{
						this.path[pacman.getXCoord()/level.getCellWidth()][pacman.getYCoord()/level.getCellHeight()]=0;
					}
				}
			// Establish origins =0 IF DEAD and NOT IN BASE 
			else if ((this.isDead()) && (this.xCoord!=level.getPhantomXOrigin() || this.yCoord!=level.getPhantomYOrigin()))
				{
				this.path[xOrigin/level.getCellWidth()][yOrigin/level.getCellHeight()]=0;
				}
			// Turn alive IF DEAD and IN BASE and establish pacman=0
			else if ((this.isDead()) && (this.xCoord==xOrigin) && (this.yCoord==yOrigin))
				{
				this.switchDead();	// Turn alive
				this.path[pacman.getXCoord()/level.getCellWidth()][pacman.getYCoord()/level.getCellHeight()]=0;
				}
				

			// Preparing the path for the phantom
			for (int z=0; z<width*height; z++)
			{
			for (int j=0; j<height; j++)
				for (int i=0; i<width; i++)
				{
					if (level.getCellValue(i,j)!=0)
					{
						this.pacPath(level, i, j);
					}
				}
			}
	}
	
	// Choosing the min value around during the propagation
	public void pacPath(Level level, int xCoord, int yCoord)
	{
		int yoffs[]={-1,0,1,0};
		int xoffs[]={0,1,0,-1};
		int width=level.getWidth();
		int height=level.getHeight();
		// finding the surrounding minimum
		for (int j=0;j<4;j++)	// 4 = number of movements available
		{
		
			int nx=(xCoord+xoffs[j]+width)%width, ny=(yCoord+yoffs[j]+height)%height;	// modulo needed to calculate path also through teleports
			if(nx>=0 && nx<width && ny>=0 && ny<=height)
			{
				this.path[xCoord][yCoord]=Math.min(this.path[xCoord][yCoord],this.path[nx][ny]+1);
			}
		}
	}
	
	
	
}	// end of class Phantom.java
