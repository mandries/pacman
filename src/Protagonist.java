import java.io.*;

import javax.swing.JOptionPane;

public class Protagonist extends Person 
	{	
	
	public Protagonist()
	{super();}
	
	public Protagonist(int xCoord, int yCoord)
	{super(xCoord,yCoord);}
	
	private int nextMoveDirection;
	public int getNextMoveDirection()
	{return nextMoveDirection;}
	public void setNextMoveDirection(int value)
	{nextMoveDirection=value;}
	
	private static int lives=5;	// Initial number of lives a pacman has
	public void setLives(int value)	// Set number of lives
	{lives=value;}
	public void addLives()	//Add 1 life
	{lives+=1;}
	public void subLives()	//Substract 1 life
	{lives-=1;}
	public int getLives()
	{return lives;}
	
	// Player score
	private static int score=0;
	
	public int getScore()
	{return score;}
	public void setScore(int value)
	{score=value;}	
	public void addToScore(int value)
	{score+=value;}
		
	// StreamReader	
	//	Save high score
	 public static void saveScore(String playerName)
	 {
		 BufferedReader streamIn = null;		
		 try 
		 {
			 // Receiving input file
			 streamIn = new BufferedReader(new FileReader("scores.txt"));
			 
			 // Reading the first line
			 String line=streamIn.readLine();
			 
			 StreamTokenizer st= new StreamTokenizer(streamIn);
			 
			 while(st.nextToken()!= StreamTokenizer.TT_EOF) {}
			 
			 
			 //Si le score est plus grand on l'enregistre
			 if(line!=null)
				 if(score>(Integer.parseInt(line)))
				 {
					 // Preparing outpout file
					 FileWriter fileOut =new FileWriter("scores.txt");
					 // Writing the new score into it
					 fileOut.write(String.valueOf(score));
		     
					 //	Pour demander le nom du jouer s'il a fait un highscore
					 JOptionPane.showMessageDialog(null,
							    "Congratulations, " + playerName +", you have established a new record! Your score is: " + score,
							    "Pacman High score achieved",
							    JOptionPane.PLAIN_MESSAGE);
					 fileOut.write("\n"+playerName);
					 fileOut.close();
				 }
			 streamIn.close();
		}
		 
		catch (FileNotFoundException e) 
		{
		   e.printStackTrace();
		} 
		
		catch (IOException e) 
		{
		   e.printStackTrace();
		}
	 }
	
	/*
	public void saveScore() throws IOException
	{
	   Reader r = new BufferedReader(new InputStreamReader(null, "scores.txt"));
	   StreamTokenizer st = new StreamTokenizer(r);

		
		FileWriter fileOut = new FileWriter("scores.txt");
		fileOut.write("1. " + score);
		fileOut.close();
	}		*/
	 
	// End of StreamReader	
		
	private int awards=1000;
	public int getAwards()
	{return awards;}
	public void setAwards(int value)
	{awards=value;}
	public void increaseAwards()
	{awards+=1000;}
}
