import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JApplet;
import javax.swing.JOptionPane;
import javax.swing.Timer;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FilePermission;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StreamTokenizer;
import java.lang.String;
import java.util.Vector;

public class Play extends JApplet implements KeyListener, ActionListener
{
	private static final long serialVersionUID = 1L;
	/*
		Protagonist pacman;
		// Greetings! Welcome to Pacman (AM)!
		// Please enter your name 
		// Save name to a variable
		// Press space to start game or H to show high-scores
		// Repeat this every 0.2 seconds untill life=0
	*/
		
	 Font 		largefont = new Font("Helvetica", Font.BOLD, 24);
	 Font		smallfont = new Font("Helvetica", Font.BOLD, 14);
	 Font 		normalfont = new Font("Helvetica", Font.PLAIN, 14);
	
	private int gameStatus=0;	// Used to pass from one game-stage to another (0=intro, 1=game, 2=game end)
	private static String playerName=null;
	private boolean messageShown=false;	// Variable used to determine whether to show or not the end-score 
	
	// Game refreshing timer
	private Timer timer;
	
	// Phantom movement timer (for managing phantom speeds and drawing)
	private int paceTimer=1;
	public void incPaceTimer()
	{
		if (paceTimer<10)
			{paceTimer++;}
		else paceTimer=1;
	}

	// User score inserted
	private boolean scoreInserted=false;
	
	// User score saved
	private boolean scoreSaved=false;
	
	private int inputKey;

	// Vector containing all the levels
	private static Vector <Level> levels = new Vector<Level>();
	private static int levelIndex=0;	// Index of the current level
	public static Level getCurrentLevel()
	{return levels.get(levelIndex);}

	Protagonist pacman = new Protagonist();
		
	// Vector for keeping all the phantoms inside
	private Vector <Phantom> phantom = new Vector<Phantom>();
		
	private boolean pause = true;	// Boolean used to determine the pause state
	private int pauseTimer=0;		// Variable used to make the "Pause" info to blink
	public void switchPause()		// Pause switcher
	{pause=!pause;}
	
	// Initializing the program
	public void init()
	{		
		
	  FilePermission perm = new java.io.FilePermission("scores.txt", "read, write");

		// Setting award barrier to 1k
		pacman.setAwards(1000);
		
		// Initializing levels
		Level level1 = new Level();
		level1.firstLevel();
		level1.setPacmanXOrigin(440);
		level1.setPacmanYOrigin(480);
		level1.setPhantomXOrigin(360);
		level1.setPhantomYOrigin(280);
		levels.add(0,level1);
		
		Level level2 = new Level();
		level2.secondLevel();
		level2.setPacmanXOrigin(400);
		level2.setPacmanYOrigin(480);
		level2.setPhantomXOrigin(360);
		level2.setPhantomYOrigin(280);
		levels.add(1,level2);
		
		Level level3 = new Level();
		level3.thirdLevel();
		level3.setPacmanXOrigin(360);
		level3.setPacmanYOrigin(360);
		level3.setPhantomXOrigin(360);
		level3.setPhantomYOrigin(280);
		levels.add(2,level3);
		
		// Setting start parameters
		pacman.setXCoord(level1.getPacmanXOrigin());
		pacman.setYCoord(level1.getPacmanYOrigin());
		pacman.setLives(3);
		pacman.setScore(0);
		
		// Initializing phantoms
		Phantom phantom1 = new Phantom(400,280);
		phantom1.setSpeed(2);
		phantom1.setReleaseTimer(0);
		phantom.add(0, phantom1);
		
		Phantom phantom2 = new Phantom(400,280);
		phantom2.setSpeed(2);
		phantom2.setReleaseTimer(7);
		phantom.add(1, phantom2);
			
		Phantom phantom3 = new Phantom(400,280);
		phantom3.setSpeed(2);
		phantom3.setReleaseTimer(14);
		phantom.add(2, phantom3);
		
		Phantom phantom4 = new Phantom(400,280);
		phantom4.setSpeed(2);
		phantom4.setReleaseTimer(21);
		phantom.add(3, phantom4);
		
		// Starting game
		addKeyListener(this);
		gameStatus=0;
		
		// Launching timer
		timer = new Timer(20,this);
		timer.start();	

		// Asking player name
		if (playerName==null)
		{playerName = JOptionPane.showInputDialog(null, "Please enter your name:", "Titre: Nom du joueur", JOptionPane.QUESTION_MESSAGE);} //.toUpperCase();}
	}
	
	// What to do when an action is performed
	public void actionPerformed(ActionEvent arg0) 
	{
		if (!pause && gameStatus<2)
			{analyze();}
		else
			{repaint();}
	}
		 
	// Function used to test if an INT is a direction
	public boolean isDirection(int inputKey)
	{
		return (inputKey == java.awt.event.KeyEvent.VK_UP || 
				inputKey == java.awt.event.KeyEvent.VK_DOWN || 
				inputKey == java.awt.event.KeyEvent.VK_LEFT || 
				inputKey == java.awt.event.KeyEvent.VK_RIGHT);
	}

	public void keyPressed (KeyEvent e) 
	{
		inputKey = e.getKeyCode();
		if (inputKey == java.awt.event.KeyEvent.VK_ESCAPE) {System.exit(0);}	// Escape game
		
		// Intro
		if (gameStatus==0)	
		{
			// Press SPACE to start game
			if (inputKey == java.awt.event.KeyEvent.VK_SPACE) 
			{
				gameStatus++; 
				pause=false;
			}
		}
		
		// In game
		else if (gameStatus==1)
		{
			if 	(inputKey == java.awt.event.KeyEvent.VK_P)	{switchPause();}	// Pause on/off
			
			// Memorize the next intended move
			if (isDirection(inputKey)) 
			{
				// Saving as the next move direction if the move is not available right away
				if (!pacman.availableMove(levels.get(levelIndex), inputKey)) 
					{pacman.setNextMoveDirection(inputKey);}
				// Else change current move direction and reset next move direction
				else if (inputKey!=pacman.getMoveDirection())
				{
					pacman.setMoveDirection(inputKey);
					pacman.setNextMoveDirection(0);	// For not to come back when we change the main direction to the opposite of the nextMoveDirection
				}
			}
		}
		
		// In high scores
		else if (gameStatus==2)
		{
			// Restart game
			if (inputKey == java.awt.event.KeyEvent.VK_SPACE)
			{
				gameStatus=0;
				levelIndex=0;
				levels.get(0).firstLevel();
				levels.get(1).secondLevel();
				levels.get(2).thirdLevel();
				scoreInserted=false;
				scoreSaved=false;
				pacman.setLives(3);
				pacman.setScore(0);
			}
		}
	}	
	
	// Function performing all the game calculations
	public void analyze()
	{
		Level currentLevel = levels.get(levelIndex);		
		
		if (pacman.getLives()<1)	// Case if dead
			{
				pause=true;
				gameStatus=2;	// Go on to game scores				
			}	
		
		// If all dots in the current level have been eaten
		if (!currentLevel.existDots())
		{	
			// Game ended (if it was the last level that has been played) --> advance to game scores
			if (levelIndex==levels.size()-1)
				{
					pause=true;
					gameStatus=2;
				}			
			// Beginning of a new level
			else
			{
				gameStatus=0;	// setting the "don't start before SPACE pressed" option

				// Resetting all the phantoms
				for (int i=0;i<phantom.size();i++)
				{
					Phantom phantomCur = phantom.get(i);
					
					phantomCur.setAwardTimer(0);			// Resetting the award timers
					phantomCur.setCherryTimer(0);		// Resetting the cherry timers
					phantomCur.setReleaseTimer(i*10);	// Keeping the phantoms in their home for a while
					
					// Placing the phantoms in their home
					phantomCur.setXCoord(currentLevel.getPhantomXOrigin());
					phantomCur.setYCoord(currentLevel.getPhantomYOrigin());					
				}

				levelIndex++;	// increasing the level number
				currentLevel = levels.get(levelIndex);	// obtaining the next level
				pause=true;	// pausing the game
				
				pacman.setMoveDirection(0);	// Resetting pacman's move direction				
				// Placing the pacman where he should be at the beginning of the level
				pacman.setXCoord(currentLevel.getPacmanXOrigin());	
				pacman.setYCoord(currentLevel.getPacmanYOrigin());
			}
		}
		
		// Increasing pace timer
		incPaceTimer();

		// Phantom movement, depending on pace timer
		for (int i=0; i<phantom.size(); i++)
		{
			Phantom phantomCur = phantom.get(i);
			
			/*
			 * We could have used less function calls
			 * but we keep the current structure for now
			 * as the program is simple and quick
			 * and the code is more comprehensible as it is
			int xCoord = phantomCur.getXCoord();
			int yCoord = phantomCur.getYCoord();
			int cellWidth = currentLevel.getCellWidth();
			int cellHeight = currentLevel.getCellHeight();
			int xCoordtab = xCoord/cellWidth;
			int yCoordtab = yCoord/cellHeight;
			 */
			
			// Decrease Cherry Timer if activated
			if (phantomCur.getCherryTimer()>0) {phantomCur.decreaseCherryTimer();}
						
			// Phantom thinks and moves depending on the state of the paceTimer
			if (paceTimer % phantomCur.getSpeed()==0)
			{
				// Phantom movement	(choose path 				
				if ((phantomCur.getXCoord() % currentLevel.getCellWidth() ==0) &&
					(phantomCur.getYCoord() % currentLevel.getCellHeight()==0))
				{
					// Separate these two to manipulate the brains and speed separately 
					phantomCur.initPacPath(currentLevel, pacman);
					phantomCur.movePhantom(currentLevel);
				}
				// Continue move till we reach next cell
				else
				{
					phantomCur.movePerson(currentLevel, phantomCur.getMoveDirection());
				}
			}// end of phantom brains
			
			
			// Test of phantom-pacman meeting
			if ((Math.abs((phantomCur.getXCoord()-pacman.getXCoord()))<currentLevel.getCellWidth()) && 
				(Math.abs((phantomCur.getYCoord()-pacman.getYCoord()))<currentLevel.getCellHeight()))
					{
					// Pacman hunted by phantoms
					if (phantomCur.getCherryTimer()==0 && !phantomCur.isDead())
						{
						pacman.subLives();	// decrease lives
						pacman.setMoveDirection(0);	// reset move direction
						pacman.setXCoord(currentLevel.getPacmanXOrigin());	// reset pacman placement
						pacman.setYCoord(currentLevel.getPacmanYOrigin());
						for (int x=0; x<phantom.size(); x++)
							{
							Phantom phantomTmp=phantom.get(x);
							phantomTmp.setCherryTimer(0);		// Reset cherry timers
							// Reset phantoms' placement
							phantomTmp.setXCoord(currentLevel.getPhantomXOrigin());
							phantomTmp.setYCoord(currentLevel.getPhantomYOrigin());
							phantomTmp.setReleaseTimer(x*8);	// Reset release timers
							}
						}
					// Pacman hunter
					else if ((phantomCur.getCherryTimer()>0) && (!phantomCur.isDead()))
						{
						// Counting number of phantoms killed in a row
						int phantomsKilled=1;
						for (int k=0; k<phantom.size(); k++)
							{
								if (phantom.get(k).isDead())
									phantomsKilled*=2;
							}
						phantomCur.setAwardTimer(30);					// including the award timer
						phantomCur.setAwardScore(phantomsKilled*200);	// Saving the number of awarded points to display them later
						phantomCur.setXKilled(phantomCur.getXCoord());	// Saving the death place of the phantom
						phantomCur.setYKilled(phantomCur.getYCoord());
						pacman.addToScore(phantomsKilled*200);			//  Awarding points
						phantomCur.setCherryTimer(0);					// Resetting cherry timer for the killed phantom
						phantomCur.switchDead();						// Changing phantom living state
						phantomCur.setReleaseTimer(i*7+5);				// Resetting phantom release timer
						}
					}
		}
		
		
		// Following next move direction when it becomes available
		if (pacman.availableMove(currentLevel, pacman.getNextMoveDirection()) &&
			isDirection(pacman.getNextMoveDirection())) 
			{pacman.setMoveDirection(pacman.getNextMoveDirection()); pacman.setNextMoveDirection(0);}
		
		// Following move direction
		if (pacman.availableMove(currentLevel, pacman.getMoveDirection())) 
			{			
			pacman.movePerson(currentLevel, pacman.getMoveDirection());
			
			// Eat to the left-up
			if (pacman.getXCoord()%currentLevel.getCellWidth()<currentLevel.getCellWidth()/2 &&
				pacman.getYCoord()%currentLevel.getCellHeight()<currentLevel.getCellHeight()/2) 
				{
				if (currentLevel.getCellValue(	pacman.getXCoord()/currentLevel.getCellWidth(),
												pacman.getYCoord()/currentLevel.getCellHeight())==2) // if simple dot
					{pacman.addToScore(10);}	// award points
				else if (currentLevel.getCellValue(	pacman.getXCoord()/currentLevel.getCellWidth(),
													pacman.getYCoord()/currentLevel.getCellHeight())==3) // if cherry
					{
					pacman.addToScore(50);	// award points
					for (int i=0; i<phantom.size(); i++)
						{
						Phantom phantomCur=phantom.get(i);
						if (!phantomCur.isDead())
							{
							phantomCur.setCherryTimer(200);		// setting phantoms' cherry timers
							}
						}
					}
				currentLevel.setCellValue(pacman.getXCoord(), pacman.getYCoord(), 1);	// Leave empty cell behind
				}
			
			// Left-down
			if (pacman.getXCoord()%currentLevel.getCellWidth()<currentLevel.getCellWidth()/2 && 
				pacman.getYCoord()%currentLevel.getCellHeight()>currentLevel.getCellHeight()/2) 
				{
				if (currentLevel.getCellValue(	pacman.getXCoord()/currentLevel.getCellWidth(),
												pacman.getYCoord()/currentLevel.getCellHeight() +1)==2)	// if simple dot 
					{pacman.addToScore(10);}	// award points
				else if (currentLevel.getCellValue(	pacman.getXCoord()/currentLevel.getCellWidth(),
													pacman.getYCoord()/currentLevel.getCellHeight() +1)==3) // if cherry
					{
					pacman.addToScore(50);
					for (int i=0; i<phantom.size(); i++)
						{
						Phantom phantomCur= phantom.get(i);
						if (!phantomCur.isDead())
							{
							phantomCur.setCherryTimer(200);
							}
						}
					}
				currentLevel.setCellValue(pacman.getXCoord(), pacman.getYCoord()+currentLevel.getCellHeight(), 1);	// Leave empty cell behind
				}
		
			// Right-up
			if (pacman.getXCoord()%currentLevel.getCellWidth()>currentLevel.getCellWidth()/2 && 
				pacman.getYCoord()%currentLevel.getCellHeight()<currentLevel.getCellHeight()/2)
				{
				if (currentLevel.getCellValue(	pacman.getXCoord()/currentLevel.getCellWidth()+1,
												pacman.getYCoord()/currentLevel.getCellHeight())==2)	// if simple dot
					{pacman.addToScore(10);}	// award points
				else if (currentLevel.getCellValue(	pacman.getXCoord()/currentLevel.getCellWidth()+1,
													pacman.getYCoord()/currentLevel.getCellHeight())==3) // if cherry
					{
					pacman.addToScore(50);
					for (int i=0; i<phantom.size(); i++)
						{
						Phantom phantomCur= phantom.get(i);
						if (!phantomCur.isDead())
							{
							phantomCur.setCherryTimer(200);
							}
						}
					}
				currentLevel.setCellValue(pacman.getXCoord()+currentLevel.getCellWidth(), pacman.getYCoord(), 1);	// Leave empty cell behind
				}
		
			// Right-down
			if (pacman.getXCoord()%currentLevel.getCellWidth()>currentLevel.getCellWidth()/2 && 
				pacman.getYCoord()%currentLevel.getCellHeight()>currentLevel.getCellHeight()/2)
				{
				if (currentLevel.getCellValue(	pacman.getXCoord()/currentLevel.getCellWidth()+1,
												pacman.getYCoord()/currentLevel.getCellHeight()+1)==2)	// if simple dot 
					{pacman.addToScore(10);}
				else if (currentLevel.getCellValue(	pacman.getXCoord()/currentLevel.getCellWidth()+1,
													pacman.getYCoord()/currentLevel.getCellHeight()+1)==3) // if cherry
					{
					pacman.addToScore(50);
					for (int i=0; i<phantom.size(); i++)
						{
						Phantom phantomCur= phantom.get(i);
						if (!phantomCur.isDead())
							{
							phantomCur.setCherryTimer(200);
							}
						}
					}
				currentLevel.setCellValue(pacman.getXCoord()+currentLevel.getCellWidth(), pacman.getYCoord()+currentLevel.getCellHeight(), 1);	// Leave empty cell behind
				}
		
			// Check if it's time to award a life to the player
			if (pacman.getScore()>pacman.getAwards())
				{
				pacman.addLives();	// Add 1 life
				pacman.increaseAwards();	// Increase requirements for the life that will be awarded next
				}
			}			
		repaint();	// call paint
	}	// end of analyze method
	
	public void keyReleased (KeyEvent e) {}
	public void keyTyped (KeyEvent e) {}
	
	public void paint(Graphics g) // The function used to draw the shapes
	{
		setFocusable(true);
		Image offImage = createImage(this.getWidth(), this.getHeight());
		try {
			bufferPaint(offImage.getGraphics());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		g.drawImage(offImage, 0, 0, this);
		requestFocus();
	}
	
	// Double buffering
	public void bufferPaint(Graphics g) throws IOException 
	{
		Level currentLevel = levels.get(levelIndex);
		
		// Dessiner la carte
		for (int j=0;j<currentLevel.getHeight();j++)
			for (int i=0;i<currentLevel.getWidth();i++)
			{
				if (currentLevel.getCellValue(i,j)==0) /*Wall*/ 
				{
					//g.setColor(Color.black);
					Color wallColor = new Color((levelIndex*10)%255,(levelIndex*20)%255,(levelIndex*30)%255);
					g.setColor(wallColor);
					g.fillRect(i*40,j*40,currentLevel.getCellWidth(),currentLevel.getCellHeight());
				}
				else if (currentLevel.getCellValue(i,j)==1) /*Emtpy Cell*/ {g.setColor(Color.gray); g.fillRect(i*40,j*40,currentLevel.getCellWidth(),currentLevel.getCellHeight());}
				else if (currentLevel.getCellValue(i,j)==2) /*Dot*/
						{
						g.setColor(Color.gray); g.fillRect(i*currentLevel.getCellWidth(),j*currentLevel.getCellHeight(),currentLevel.getCellWidth(),currentLevel.getCellHeight());
						g.setColor(Color.black); g.fillOval(i*currentLevel.getCellWidth()+18,j*currentLevel.getCellHeight()+18,4,4);						
						}
				else if (currentLevel.getCellValue(i,j)==3) /*Cherry*/
				{
					g.setColor(Color.gray); g.fillRect(i*currentLevel.getCellWidth(),j*currentLevel.getCellHeight(),currentLevel.getCellWidth(),currentLevel.getCellHeight());
					g.setColor(Color.red); 
					g.fillOval(i*currentLevel.getCellWidth()+5,j*currentLevel.getCellHeight()+26,8,8);
					g.fillOval(i*currentLevel.getCellWidth()+25,j*currentLevel.getCellHeight()+30,8,8);
					
					// White reflection
					Color paleWhite = new Color (255,255,255,90);
					g.setColor(paleWhite);
					g.fillOval(i*currentLevel.getCellWidth()+6,j*currentLevel.getCellHeight()+27,4,4);
					g.fillOval(i*currentLevel.getCellWidth()+26,j*currentLevel.getCellHeight()+31,4,4);
					
					
					g.setColor(Color.green);
					g.drawLine(	i*currentLevel.getCellWidth()+11,j*currentLevel.getCellHeight()+25, 
								i*currentLevel.getCellWidth()+27,j*currentLevel.getCellHeight()+5);
					g.drawLine(	i*currentLevel.getCellWidth()+28,j*currentLevel.getCellHeight()+29, 
								i*currentLevel.getCellWidth()+27,j*currentLevel.getCellHeight()+5);
				}				
			}
		
		// Dessiner Pacman
		// Head
		g.setColor(Color.yellow);
		g.fillOval(pacman.getXCoord(), pacman.getYCoord(), pacman.getWidth(), pacman.getHeight());
		// Eyes
		if (pacman.getMoveDirection()==java.awt.event.KeyEvent.VK_RIGHT || 
			pacman.getMoveDirection()==java.awt.event.KeyEvent.VK_LEFT)
		{	
			// Sclerotica
			g.setColor(Color.white);				
			g.fillOval(pacman.getXCoord()+16, pacman.getYCoord()+8, 10, 10);			

			// Iris										
			g.setColor(Color.black);
			g.fillOval(pacman.getXCoord()+17, pacman.getYCoord()+11, 6, 6);
		}
		else
		{
			if (pacman.getMoveDirection()==java.awt.event.KeyEvent.VK_UP)
			{				
				// Sclerotica
				g.setColor(Color.white);				
				g.fillOval(pacman.getXCoord()+24, pacman.getYCoord()+16, 10, 10);
				// Iris	
				g.setColor(Color.black);
				g.fillOval(pacman.getXCoord()+26, pacman.getYCoord()+16, 6, 6);
			}
			else if (pacman.getMoveDirection()==java.awt.event.KeyEvent.VK_DOWN)
			{
				// Sclerotica
				g.setColor(Color.white);				
				g.fillOval(pacman.getXCoord()+6, pacman.getYCoord()+8, 10, 10);
				// Iris	
				g.setColor(Color.black);
				g.fillOval(pacman.getXCoord()+8, pacman.getYCoord()+12, 6, 6);
			}
			// Standing still
			else
			{
				// Sclerotica
				g.setColor(Color.white);				
				g.fillOval(pacman.getXCoord()+8, pacman.getYCoord()+10, 8, 8);
				g.fillOval(pacman.getXCoord()+24, pacman.getYCoord()+10, 8, 8);
				// Iris
				g.setColor(Color.black);
				g.fillOval(pacman.getXCoord()+9, pacman.getYCoord()+11, 6, 6);
				g.fillOval(pacman.getXCoord()+25, pacman.getYCoord()+11, 6, 6);
			}
		}
		// Biting Mouth
		if (paceTimer>5)
		{
			g.setColor(Color.gray);
			int xCenter = pacman.getXCoord()+currentLevel.getCellWidth()/2;
			int yCenter = pacman.getYCoord()+currentLevel.getCellHeight()/2;
			int x1=0,x2=0,y1=0,y2=0;

			switch(pacman.getMoveDirection())
			{
			case java.awt.event.KeyEvent.VK_RIGHT:	{
													x1= pacman.getXCoord()+currentLevel.getCellWidth();
													y1= pacman.getYCoord()+(currentLevel.getCellHeight()/4);
													x2= pacman.getXCoord()+currentLevel.getCellWidth();
													y2= pacman.getYCoord()+3*currentLevel.getCellHeight()/4;
													break;													
													}
			case java.awt.event.KeyEvent.VK_LEFT:	{													
													x1= pacman.getXCoord();
													y1= pacman.getYCoord()+currentLevel.getCellHeight()/4;
													x2= pacman.getXCoord();
													y2= pacman.getYCoord()+3*currentLevel.getCellHeight()/4;
													break;
													}
			case java.awt.event.KeyEvent.VK_UP:		{
													x1= pacman.getXCoord()+currentLevel.getCellWidth()/4;
													y1= pacman.getYCoord();
													x2= pacman.getXCoord()+3*currentLevel.getCellWidth()/4;
													y2= pacman.getYCoord();
													break;
													}
			case java.awt.event.KeyEvent.VK_DOWN:	{
													x1= pacman.getXCoord()+currentLevel.getCellWidth()/4;
													y1= pacman.getYCoord()+currentLevel.getCellHeight();
													x2= pacman.getXCoord()+3*currentLevel.getCellWidth()/4;
													y2= pacman.getYCoord()+currentLevel.getCellHeight();
													break;
													}
			}
			
			int[] xPoints={x1, xCenter, x2};
			int[] yPoints={y1, yCenter, y2};
			g.fillPolygon(xPoints, yPoints, 3);
		}
		
		// Dessiner les phantomes	(Inky, Pinky, Dinky, Clyde)
		for (int i=0; i<phantom.size(); i++)
		{
			Phantom phantomCur = phantom.get(i);

			// Showing awarded points for the dead phantoms
			if (phantomCur.getAwardTimer()>0 && gameStatus==1)
			{
				g.setFont(smallfont);
				g.setColor(Color.black);
				g.drawString("" + phantomCur.getAwardScore(), phantomCur.getXKilled()+10, phantomCur.getYKilled()+25);
				g.setColor(Color.pink);
				g.drawString("" + phantomCur.getAwardScore(), phantomCur.getXKilled()+11,phantomCur.getYKilled()+25);
				phantomCur.decAwardTimer();			
			}
			
			// If phantom not dead --> show it
			if (!phantomCur.isDead())
			{	
				// If phantom chased -> color blue and draw scarried mouth	
				if (phantomCur.getCherryTimer()>50 || phantomCur.getCherryTimer()%2==1) 
				{
					g.setColor(Color.blue);
				}
				// else their natural color
				else	
					{
					if (i==0)// Pinky
						g.setColor(Color.pink);
					else if (i==1)// Blinky
						g.setColor(Color.green);
					else if (i==2) // Klyde
						g.setColor(Color.red);
					else if (i==3) // Dinky
						g.setColor(Color.orange);
					}				
				// Head
				g.fillOval(phantomCur.getXCoord(), phantomCur.getYCoord(), phantomCur.getWidth(), phantomCur.getHeight());
				g.fillRect(phantomCur.getXCoord(), phantomCur.getYCoord()+20, phantomCur.getWidth(), phantomCur.getHeight()-20);
				
				// Tentacles
				g.setColor(Color.gray);

				if (paceTimer%12>6)
				{
					int[] xPoints = { phantomCur.getXCoord(), 	phantomCur.getXCoord(),
							phantomCur.getXCoord()+5, 	
							phantomCur.getXCoord()+10, phantomCur.getXCoord()+15, phantomCur.getXCoord()+20,
							phantomCur.getXCoord()+25, phantomCur.getXCoord()+30, phantomCur.getXCoord()+35, 
							phantomCur.getXCoord()+40, phantomCur.getXCoord()+40};
					int[] yPoints = { phantomCur.getYCoord()+40, phantomCur.getYCoord()+35, 	
							phantomCur.getYCoord()+40,phantomCur.getYCoord()+35,
							phantomCur.getYCoord()+40, phantomCur.getYCoord()+35,
							phantomCur.getYCoord()+40, phantomCur.getYCoord()+35, 
							phantomCur.getYCoord()+40, phantomCur.getYCoord()+35, phantomCur.getYCoord()+40,};
					g.fillPolygon(xPoints, yPoints, 11);
				}
				else 
				{
					int[] xPoints = { phantomCur.getXCoord(), 	phantomCur.getXCoord()+5, 	
							phantomCur.getXCoord()+10,	phantomCur.getXCoord()+15,
							phantomCur.getXCoord()+20, phantomCur.getXCoord()+25,
							phantomCur.getXCoord()+30, phantomCur.getXCoord()+35, phantomCur.getXCoord()+40, phantomCur.getXCoord()+40};
					int[] yPoints = { phantomCur.getYCoord()+40,	phantomCur.getYCoord()+35, 	
							phantomCur.getYCoord()+40,phantomCur.getYCoord()+35,
							phantomCur.getYCoord()+40, phantomCur.getYCoord()+35,
							phantomCur.getYCoord()+40, phantomCur.getYCoord()+35, phantomCur.getYCoord()+40};
					g.fillPolygon(xPoints, yPoints, 9);
				}
				
				// Mouth
				if (phantomCur.getCherryTimer()>0)
				{
					g.setColor(Color.white);
					g.drawLine(phantomCur.getXCoord()+8, phantomCur.getYCoord()+28, phantomCur.getXCoord()+12, phantomCur.getYCoord()+24);
					g.drawLine(phantomCur.getXCoord()+12, phantomCur.getYCoord()+24, phantomCur.getXCoord()+16, phantomCur.getYCoord()+28);
					g.drawLine(phantomCur.getXCoord()+16, phantomCur.getYCoord()+28, phantomCur.getXCoord()+20, phantomCur.getYCoord()+24);
					g.drawLine(phantomCur.getXCoord()+20, phantomCur.getYCoord()+24, phantomCur.getXCoord()+24, phantomCur.getYCoord()+28);
					g.drawLine(phantomCur.getXCoord()+24, phantomCur.getYCoord()+28, phantomCur.getXCoord()+28, phantomCur.getYCoord()+24);
					g.drawLine(phantomCur.getXCoord()+28, phantomCur.getYCoord()+24, phantomCur.getXCoord()+32, phantomCur.getYCoord()+28);
				}
			}


			// Eyes
			// Sclerotica
			g.setColor(Color.white);				
			g.fillOval(phantomCur.getXCoord()+8, phantomCur.getYCoord()+10, 8, 8);
			g.fillOval(phantomCur.getXCoord()+24, phantomCur.getYCoord()+10, 8, 8);

			if (phantomCur.getCherryTimer()==0 || phantomCur.isDead())
			{
				// Iris										
				g.setColor(Color.black);
				switch(phantomCur.getMoveDirection())
				{
				case java.awt.event.KeyEvent.VK_RIGHT:	{
														g.fillOval(phantomCur.getXCoord()+12, phantomCur.getYCoord()+12, 6, 6);
														g.fillOval(phantomCur.getXCoord()+28, phantomCur.getYCoord()+12, 6, 6);
														break;
														}
				case java.awt.event.KeyEvent.VK_LEFT:	{
														g.fillOval(phantomCur.getXCoord()+8, phantomCur.getYCoord()+12, 6, 6);
														g.fillOval(phantomCur.getXCoord()+24, phantomCur.getYCoord()+12, 6, 6);
														break;
														}
				case java.awt.event.KeyEvent.VK_UP:		{
														g.fillOval(phantomCur.getXCoord()+9, phantomCur.getYCoord()+9, 6, 6);
														g.fillOval(phantomCur.getXCoord()+25, phantomCur.getYCoord()+9, 6, 6);
														break;
														}
				case java.awt.event.KeyEvent.VK_DOWN:	{
														g.fillOval(phantomCur.getXCoord()+9, phantomCur.getYCoord()+13, 6, 6);
														g.fillOval(phantomCur.getXCoord()+25, phantomCur.getYCoord()+13, 6, 6);
														break;
														}
				}
			}
		}
		
		// Show Data-board
		Color boardColor = new Color(125,225,125);
		g.setColor(boardColor);
		g.fillRect(0,currentLevel.getHeight()*currentLevel.getCellHeight(),currentLevel.getWidth()*currentLevel.getCellWidth(),30);

		// Choosing font colour
		g.setColor(Color.black);
		
		// Show default data:
		g.setFont(normalfont);
		g.drawString("Player: ", 25, currentLevel.getHeight()*currentLevel.getCellHeight()+18);
		g.drawString("Score: ", 310, currentLevel.getHeight()*currentLevel.getCellHeight()+18);
		g.drawString("Lives: ", 440,currentLevel.getHeight()*currentLevel.getCellHeight()+18);
		g.drawString("Level:", 650,currentLevel.getHeight()*currentLevel.getCellHeight()+18);
		
		//	Show user data
		g.setFont(smallfont);
		g.drawString(""+playerName, 70, currentLevel.getHeight()*currentLevel.getCellHeight()+18);
		g.drawString("" + pacman.getScore(), 355, currentLevel.getHeight()*currentLevel.getCellHeight()+18);
		g.drawString("" + pacman.getLives(), 480,currentLevel.getHeight()*currentLevel.getCellHeight()+18);
		int boardLevel = levelIndex+1;
		g.drawString("0" + boardLevel +"/0"+levels.size(), 690,currentLevel.getHeight()*currentLevel.getCellHeight()+18);

		// Show pause		
		if (pause && gameStatus==1)
		{
			pauseTimer++;
			if (pauseTimer<20)
				{
				g.setColor(Color.gray);
				g.fillRoundRect(350, 267, 100, 60, 5,5);
				g.setColor(Color.black);
				g.drawRoundRect(350, 267, 100, 60, 5,5);
				g.setColor(Color.white);
				g.setFont(smallfont);
				g.drawString("En pause", 370, 300);
				}
			else if (pauseTimer==40)
				{pauseTimer=0;}
		}

		/*
		// Drawing distances on map (Dijkstra)
		for (int k=0; k<phantom.size(); k++)
		{
			Phantom phantomCur = phantom.get(k);
			System.out.println("Showing distance map:");
			System.out.println("");
			for (int j=0; j<currentLevel.getHeight(); j++)		
			{
				for (int i=0; i<currentLevel.getWidth(); i++)
				{
					if (phantomCur.path[i][j]>=100)
						{System.out.print(phantomCur.path[i][j] + "   ");}
					else if (phantomCur.path[i][j]>=10)
					{System.out.print(phantomCur.path[i][j] + "    ");}
					else
					{System.out.print(phantomCur.path[i][j] + "     ");}
				}
				System.out.println("");	
			}
			System.out.println("End");
		}
		*/
		
		// Show intro panel		
		if (gameStatus==0)
		{
			// Background rectangle
			Color overlay = new Color(52,66,147, 80);
			g.setColor(overlay);
			g.fillRect(currentLevel.getWidth()*currentLevel.getCellWidth()/4-40, currentLevel.getHeight()*currentLevel.getCellHeight()/4, 
						currentLevel.getWidth()*currentLevel.getCellWidth()/2+80, currentLevel.getHeight()*currentLevel.getCellHeight()/2);
			g.setColor(Color.white);
			g.drawRect(currentLevel.getWidth()*currentLevel.getCellWidth()/4-40, currentLevel.getHeight()*currentLevel.getCellHeight()/4, 
					currentLevel.getWidth()*currentLevel.getCellWidth()/2+80, currentLevel.getHeight()*currentLevel.getCellHeight()/2);
			
			// Title
			g.setFont(largefont);
			g.setColor(boardColor);
			g.drawString("Pacman",355, currentLevel.getHeight()*currentLevel.getCellHeight()/2-115);
		    g.setFont(smallfont);
		    
		    // Instructions
		    g.setColor(Color.white);
		    g.drawString("Direct the pacman and collect all the dots and cherries.", 210, currentLevel.getHeight()*currentLevel.getCellHeight()/2-80);
		    g.drawString("Avoid phantoms to stay alive.", 210, currentLevel.getHeight()*currentLevel.getCellHeight()/2-50);
		    g.drawString("Eat cherries to become a predator.", 210, currentLevel.getHeight()*currentLevel.getCellHeight()/2-20);		    
		    g.drawString("Controls: arrows", 325, currentLevel.getHeight()*currentLevel.getCellHeight()/2+10);
		    g.drawString("P: Pause", 325, currentLevel.getHeight()*currentLevel.getCellHeight()/2+40);
		    g.drawString("Esc: Exit game", 325, currentLevel.getHeight()*currentLevel.getCellHeight()/2+70);
			g.drawString("Press SPACE to start", 325, currentLevel.getHeight()*currentLevel.getCellHeight()/2+100);
		}
		else if (gameStatus==2)
		{
		// If score not shown yet --> show it
		if (!messageShown)	
			{
			messageShown=true;
			if (pacman.getLives()>0)
				{
				JOptionPane.showMessageDialog(null,
					    "Congratulations, " + playerName +", you won! Your score is: " + pacman.getScore(),
					    "Game score",
					    JOptionPane.PLAIN_MESSAGE);
				}
			else	// if Game over caused by death 
				{
				JOptionPane.showMessageDialog(null,
					    "Game over, " + playerName + ". Score: " + pacman.getScore(), 
					    "Game over",
					    JOptionPane.ERROR_MESSAGE);
				}			
			}
		
		// High scores
		// Background rectangle
		Color overlay = new Color(52,66,147, 80);
		g.setColor(overlay);
		g.fillRect(currentLevel.getWidth()*currentLevel.getCellWidth()/4-40, currentLevel.getHeight()*currentLevel.getCellHeight()/4, 
					currentLevel.getWidth()*currentLevel.getCellWidth()/2+80, currentLevel.getHeight()*currentLevel.getCellHeight()/2);
		g.setColor(Color.white);
		g.drawRect(currentLevel.getWidth()*currentLevel.getCellWidth()/4-40, currentLevel.getHeight()*currentLevel.getCellHeight()/4, 
				currentLevel.getWidth()*currentLevel.getCellWidth()/2+80, currentLevel.getHeight()*currentLevel.getCellHeight()/2);
							
		// Title "High scores"
		g.setFont(largefont);
		g.setColor(boardColor);
		g.drawString("High scores",315, currentLevel.getHeight()*currentLevel.getCellHeight()/2-115);
				
		// Show high scores
		g.setColor(Color.white);
		g.setFont(smallfont);
		
		String toWrite="";
		try 
			{
				FileReader fr = new FileReader("scores.txt");
			    BufferedReader br = new BufferedReader(fr);
			    StreamTokenizer st = new StreamTokenizer(br);
				
			    st.ordinaryChar('.');
			    st.wordChars('\'', '\'');			    
	
			    int lastLineNo=0;
			    int y=200;
			    String tmpSval=null;
			    while (st.nextToken() != StreamTokenizer.TT_EOF && st.lineno()<11) //  || st.nval>pacman.getScore()) 
			    {
			        switch (st.ttype) 
			        {
			        case StreamTokenizer.TT_WORD:
			        	if (st.lineno()!=lastLineNo)
			        	{
			        		y+=15;
			        	}
		        		tmpSval=st.sval;
		              	break;
			        case StreamTokenizer.TT_NUMBER:
			        	if (st.lineno()!=lastLineNo)
			        	{
			        		y+=15;
			        		g.drawString(st.lineno() + ") ", 275, y);
			        	}
			        	if (st.nval>pacman.getScore())
			        		{
			        		g.drawString(st.lineno() + ") ", 275, y);
			        		g.drawString(tmpSval + " ", 310, y);
			        		g.drawString(((int) st.nval) + "", 450, y);
			        		toWrite+=tmpSval + " " + st.nval + "\n";
			        		}		        		
			        	if (st.nval<=pacman.getScore())
			        	{
			        		if (!scoreInserted)
			        		{
				        		g.drawString(st.lineno()-1 + ") ", 275, y);
				        		g.drawString("" + playerName + " ", 310, y);
				        		g.drawString("" + pacman.getScore() + "", 450, y);
				        		toWrite+=playerName+" " + pacman.getScore() + "\n";
			        			scoreInserted=true;
				        		y+=15;
			        		}
			        		
			        		if (scoreInserted)
			        		{
				        		g.drawString((st.lineno()) + ") ", 275, y);
				        		g.drawString(tmpSval + " ", 310, y);
				        		g.drawString(((int) st.nval) + "", 450, y);
				        		toWrite+=tmpSval + " " + st.nval + "\n";			        			
			        		}
			        		// if your score not shown
			        			// show it
			        		// show other lines from file with st.lineno()+1
			        	}
		               	break;
			        default:
			        	System.out.println(st.lineno() + ") " + (char) st.ttype);
			        }	// end of switch (read word type)
	        		lastLineNo=st.lineno();
			    }	// end of while
			    fr.close();	// close fileReader
			} // end of try (read file)
			
		catch (Exception e) 
			{
				System.out.println("Exception: " + e);
			}
		
		// Save score if not already written
		if(!scoreSaved)
			{
				// write high scores
				try
				{
					FileWriter fr = new FileWriter("scores.txt");
					BufferedWriter br = new BufferedWriter(fr);
					System.out.println("toWrite (before writing to file): " +toWrite);
					br.write(toWrite);

					br.close();
					fr.close();				
					scoreSaved=true;
				}
				catch (Exception e)
				{
					System.err.println("Error: " + e.getMessage());
				}
			}	// end of if (!scoreSaved);

		}// end of if gameStatus==2
		
		// Cover the right side of the map (for hiding characters durint teleportation)
		g.setColor(Color.WHITE);
		g.fillRect(currentLevel.getWidth()*currentLevel.getCellWidth(), 0, 40, 800);
	} 	// end of method bufferPaint
	
}	// end of class Play.java