public class Person
{
	public Person()	{}
		
	public Person(int xCoord, int yCoord) 
	{
		this.xCoord = xCoord;
		this.yCoord = yCoord;
	}
	
	protected int moveDirection;
	public int getMoveDirection()
	{return moveDirection;}
	public void setMoveDirection(int value)
	{moveDirection=value;}
	
	// Person image dimensions
	protected int width=40;
	public int getWidth()
	{return width;}
	
	protected int height=40;
	public int getHeight()
	{return height;}
	
	// Movement pace
	protected int pace=5;
	
	// Placement on the map
	protected int xCoord=0;	
	public int getXCoord() 
	{
		Level level = Play.getCurrentLevel();
		int width = level.getWidth();
		int cellWidth = level.getCellWidth();
		return (xCoord+width*cellWidth)%(width*cellWidth);
	}

	public void setXCoord(int xCoord)
	{
		Level level = Play.getCurrentLevel();
		int width = level.getWidth();
		int cellWidth = level.getCellWidth();
		this.xCoord=(xCoord+width*cellWidth)%(width*cellWidth);
	}
	
	protected int yCoord=0;
	public void setYCoord(int yCoord)
	{ 
		Level level = Play.getCurrentLevel();
		int height = level.getHeight();
		int cellHeight = level.getCellHeight();
		this.yCoord=(yCoord+height*cellHeight)%(height*cellHeight);
	}
	public int getYCoord() 
	{
		Level level = Play.getCurrentLevel();
		int height = level.getHeight();
		int cellHeight = level.getCellHeight();
		return (yCoord+height*cellHeight)%(height*cellHeight);
	}
	
	// Check if input coordinates are accessible
	public boolean availableMove(Level level, int xCoord, int yCoord)
	{
		return (level.getCellValue(	(xCoord/level.getCellWidth() + level.getWidth()) % level.getWidth(),
									(yCoord/level.getCellHeight()+ level.getHeight())%level.getHeight())!=0);
	}

	// Check if cell in the input direction is accessible
	public boolean availableMove(Level level, int Direction)
	{
		switch (Direction)
		{
			case java.awt.event.KeyEvent.VK_UP		:	{
														return (this.availableMove(level, this.getXCoord(), this.getYCoord()-this.pace) &&
																this.availableMove(level,this.getXCoord()+level.getCellWidth()-1, this.getYCoord()-this.pace));														
														}// xOffset=0;	yOffset=-1;
			case java.awt.event.KeyEvent.VK_RIGHT	:	{
														return (this.availableMove(level, this.getXCoord()+level.getCellWidth(), this.getYCoord()) &&
																this.availableMove(level, this.getXCoord()+level.getCellWidth(), this.getYCoord()+level.getCellHeight()-1));
														}	// xOffset=1;	yOffset=0;
			case java.awt.event.KeyEvent.VK_DOWN	:	{
														return (this.availableMove(level, this.getXCoord(), this.getYCoord()+level.getCellHeight()) &&
																this.availableMove(level, this.getXCoord()+level.getCellWidth()-1, this.getYCoord()+level.getCellHeight())); 
														}// xOffset=0;	yOffset=1;
			case java.awt.event.KeyEvent.VK_LEFT	:	{
														return (this.availableMove(level, this.getXCoord()-this.pace, this.getYCoord())) &&
																this.availableMove(level, this.getXCoord()-this.pace, this.getYCoord()+level.getCellHeight()-1); 
														}// xOffset=-1;	yOffset=0;
		}			
		return false;
	}	

	// Move person in the input direction	
	public void movePerson(Level level, int Direction)
	{
		switch (Direction)
		{
			case java.awt.event.KeyEvent.VK_UP		:	{
														if (this.availableMove(level, this.getXCoord(), this.getYCoord()-this.pace) &&
															this.availableMove(level, this.getXCoord()+level.getCellWidth()-1, this.getYCoord()-this.pace))
															{this.setYCoord(this.getYCoord()-this.pace);}
														break;
														}// xOffset=0;	yOffset=-1;
			case java.awt.event.KeyEvent.VK_RIGHT	:	{
														if (this.availableMove(level, this.getXCoord()+level.getCellWidth(), this.getYCoord()) &&
															this.availableMove(level, this.getXCoord()+level.getCellWidth(), this.getYCoord()+level.getCellHeight()-1))
															{this.setXCoord(this.getXCoord()+this.pace);}
														
														break;
														}	// xOffset=1;	yOffset=0;
			case java.awt.event.KeyEvent.VK_DOWN	:	{
														if (this.availableMove(level, this.getXCoord(), this.getYCoord()+level.getCellHeight()) &&
															this.availableMove(level, this.getXCoord()+level.getCellWidth()-1, this.getYCoord()+level.getCellHeight()))
															{this.setYCoord(this.getYCoord()+this.pace);}
														break;
														}// xOffset=0;	yOffset=1;
			case java.awt.event.KeyEvent.VK_LEFT	:	{
														if (this.availableMove(level, this.getXCoord()-this.pace, this.getYCoord()) &&
															this.availableMove(level, this.getXCoord()-this.pace, this.getYCoord()+level.getCellHeight()-1))
															{this.setXCoord(this.getXCoord()-this.pace);}
														break;
														}// xOffset=-1;	yOffset=0;
		}		
	} // end of method movePacman	
		
}
